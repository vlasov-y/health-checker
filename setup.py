#!/usr/bin/env python3

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from os import path
from glob import iglob

here = path.abspath(path.dirname(__file__))

setup(
    name='health-checker',
    url='https://gitlab.com/vlasov-y/health-checker',
    version='1.0',
    description='Monitor URLs with curl, hosts with ping. Checks SSL expiration. Notify by email.',
    author='Yuriy Vlasov',
    author_email='yuriy@vlasov.pro',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7'
    ],
    packages=[
        path.dirname(path.relpath(i, path.join(here, 'src'))).replace('/', '.')
        for i in iglob(path.join(here, 'src/**/__init__.py'), recursive=True)
    ],
    package_dir={'': 'src'},
    install_requires=[
        'markdown',
        'requests',
        'PyYAML',
        'python-telegram-bot',
        'psutil'
    ],
    entry_points={
        'console_scripts': [
            'health-checker = health_checker:entrypoint'
        ]
    },
    zip_safe=False,
    python_requires='>=3.6'
)
