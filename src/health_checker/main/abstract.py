#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Abstract classes
"""

from abc import ABC, abstractmethod
from multiprocessing import Queue


class ASingleton(type, ABC):
    """
    Pattern Singleton - one object of the class per thread
    """
    __instances = dict()

    def __call__(cls, *args, **kwargs):
        if cls not in cls.__instances:
            cls.__instances[cls] = super(ASingleton, cls).__call__(*args, **kwargs)
        return cls.__instances[cls]


class AConfigurable(ABC):
    def __init__(self):
        self.config: Configuration = Configuration()


class AConfigurationBlock(ABC):
    def __str__(self):
        return str(self.__dict__)

    def update_dict(self, d: dict):
        if not d:
            return
        for k in self.__dict__.keys():
            self.__dict__[k] = d.get(k, self.__dict__[k])

    def __bool__(self):
        """
        True if this configuration block is present in config and have no None in fields
        :return: bool
        """
        return self.__dict__['enabled'] if 'enabled' in self.__dict__ else None not in self.__dict__.values()


class ANotificationChannel(AConfigurable, ABC):
    def __init__(self):
        super().__init__()
        self.enabled = self.config.notification.enabled
        self.interval = self.config.notification.interval

    @abstractmethod
    def notify(self, subject: str, body: str, **kwargs):
        pass


class AProcess(AConfigurable, ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def start(self, *args, **kwargs):
        """
        Method with infinite loop which will be an entrypoint for new process
        :param args:
        :param kwargs:
        :return:
        """
        pass


class AProducer(ABC):
    """
    Producer fill queue with data for consumers
    """

    def __init__(self, *, queue: Queue):
        self.queue = queue


class AConsumer(ABC):
    """
    Consumer gets data from queue and process it
    """

    def __init__(self, *, queue: Queue):
        self.queue = queue


from .configuration import Configuration
