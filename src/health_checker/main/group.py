#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List

from .abstract import AConfigurable


class Group(AConfigurable):
    def __init__(self, *, name: str):
        """
        Takes name of group from config, gets its configuration from self.config and creates Recipient
        objects according to the list from obtained configuration
        :param name: Name of group as it is in config
        """
        super().__init__()
        group_list = self.config.groups.get(name, [])
        if name == '_all' or '_all' in group_list:
            group_list = [name for name in self.config.recipients.keys()]
        self.name = name
        self.members: List[Recipient] = [Recipient(name=name) for name in group_list]
        assert self.members, f"No members in group {name}"


from .recipient import Recipient
