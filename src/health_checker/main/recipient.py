#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List

from .abstract import AConfigurable


class Recipient(AConfigurable):
    def __init__(self, *, name: str):
        """
        Takes name of recipient from config, gets its configuration from self.config and creates NotificationChannels
        objects according to the list from obtained configuration
        :param name: Name of recipient as it is in config
        """
        super().__init__()
        recipient_config = self.config.recipients.get(name)
        assert type(recipient_config) is list, f"Recipient {name} config must be list of dicts"
        assert len(recipient_config), f'No configuration channels were specified for recipient {name}'
        for i, channel in enumerate(recipient_config):
            assert type(channel) is dict, f"Notification channel config #{i+1} for recipient {name} must be dict"
        self.name = name
        # key - is type of channel, while value its options
        # example: in case of email: key=email, value=john@smith.com
        self.channels: List[ANotificationChannel] = [
            NotificationChannel(**config) for config in recipient_config
        ]

from .notification import NotificationChannel, ANotificationChannel
