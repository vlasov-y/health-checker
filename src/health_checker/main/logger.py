#!/usr/bin/env python3

import sys
import os
import inspect
import logging


class Formatter(logging.Formatter):
    def __init__(self, *, fmt: str, datefmt: str = None, style: str = '%', colored: bool = True):
        super().__init__(fmt=fmt, datefmt=datefmt, style=style)
        self.colored = colored
        self.reset_seq = "\033[0m"
        self.bold_seq = "\033[1m"
        black, red, green, yellow, blue, magenta, cyan, white = (f"\033[1;{30 + i}m" for i in range(8))
        self.colors = dict(
            debug=yellow,
            info=green,
            warning=yellow,
            error=red,
            critical=red
        )

    def format(self, record):
        levelname = record.levelname.lower()
        if self.colored:
            record.levelname = self.bold_seq + self.colors.setdefault(levelname, '') + levelname + self.reset_seq
        else:
            record.levelname = self.bold_seq + levelname + self.reset_seq
        return logging.Formatter.format(self, record)


def get_logger(name: str = None) -> logging.Logger:
    """
    Return formatted logger basing on __name__
    :param name: __name__
    :return: logging.Logger
    """
    if not name:
        # get name of module which asked logger
        frame = inspect.stack()[1]
        module = inspect.getmodule(frame[0])
        name = module.__name__
    # handle debug mode
    if '--debug' in sys.argv or os.getenv('HC_DEBUG', 'false').lower() in ('1', 'true', 'yes'):
        fmt = f'%(levelname)s:\t%(filename)s %(lineno)d:\t%(message)s'
        std_loglevel = logging.DEBUG
    else:
        fmt = f'%(asctime)s: %(levelname)s:\t%(message)s'
        std_loglevel = logging.INFO
    # create logger
    logger = logging.getLogger(name=name)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    # stdout \ stderr
    stream = logging.StreamHandler()
    stream.setLevel(std_loglevel)
    # formatter
    formatter = Formatter(fmt=fmt, datefmt='%m %b %H:%M:%S')
    # attach formatter
    stream.setFormatter(formatter)
    logger.addHandler(stream)
    return logger


if __name__ == '__main__':
    sys.exit(1)
