#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from hashlib import md5
from multiprocessing import Queue
from threading import Thread
from http.server import BaseHTTPRequestHandler, HTTPServer

from .abstract import AProcess, AConsumer

from .logger import get_logger

gauges = dict()
logger = get_logger()


class Prometheus(AProcess, AConsumer):
    class Gauge:
        def __init__(self, name: str, labels: dict):
            self.__value = None
            self.__name = ''
            self.labels = labels
            self.__name_patterns = [
                re.compile(r'[^a-zA-Z0-9_:]'),
                re.compile(r'^[^a-zA-Z_:]+')
            ]
            self.__label_patterns = [
                re.compile(r'[^a-zA-Z0-9_]'),
                re.compile(r'^[^a-zA-Z_]+')
            ]
            self.name = name

        @property
        def value(self):
            return self.__value

        @value.setter
        def value(self, value):
            if type(value) not in [float, int]:
                raise TypeError('Gauge: type(value) must be in [float, int]')
            self.__value = float(value)

        def __add__(self, value):
            if type(value) not in [float, int]:
                raise TypeError('Gauge: type(value) must be in [float, int]')
            self.__value += value

        def __sub__(self, value):
            if type(value) not in [float, int]:
                raise TypeError('Gauge: type(value) must be in [float, int]')
            self.__value -= value

        @property
        def name(self):
            return self.__name

        @name.setter
        def name(self, name):
            for p in self.__name_patterns:
                name = re.sub(p, '', name)
            self.__name = name

        def format_labels(self):
            return "{" + ','.join([f'{k}="{v}"' for k, v in self.labels.items()]) + "}"

        def __str__(self):
            return f"""
# HELP {self.name} {self.name}
# TYPE {self.name} gauge
{self.name}{self.format_labels()} {self.value}
            """.strip()

        def __hash__(self):
            hash_md5 = md5()
            hash_md5.update(self.name.encode())
            for name, value in self.__dict__.items():
                hash_md5.update(f'{name}{value}'.encode())
            return hash(hash_md5.hexdigest())

    def __init__(self, queue: Queue):
        AProcess.__init__(self)
        AConsumer.__init__(self, queue=queue)
        logger.debug('Prometheus: object created')

    def update_stats(self, **kwargs):
        """
        Parse any dict and create gauges for deepest items
        Example dict:
        {"host": {"subsystem": {"stat1": 1234, "stat2": 5678 } } }
        Will produce gauges:
        host_subsystem_stat1 1234
        host_subsystem_stat2 5678
        :param prefix: Prefix string for gauge names
        :param metrics: Dict with stats
        :return:
        """

        def recursive_processing(gauges, prefix, metrics, labels):
            for k, v in metrics.items():
                if prefix[-1] != '_':
                    prefix = f'{prefix}_'
                if type(v) is dict:
                    recursive_processing(gauges=gauges, prefix=prefix, metrics=metrics, labels=labels)
                elif type(v) in [float, int]:
                    gauge = Prometheus.Gauge(name=f'{prefix}{k}', labels=labels)
                    h = hash(gauge)
                    if h not in gauges:
                        gauges[h] = gauge
                    else:
                        del gauge
                        gauge = gauges[h]
                    # update gauge value
                    gauge.value = v

        if not self.config.prometheus.enabled:
            return  # skip
        # if prometheus enabled
        metrics = kwargs.setdefault('metrics', {})
        prefix = kwargs.setdefault('prefix', 'healthchecker_')
        labels = kwargs.setdefault('labels', {})
        if type(labels) is not dict:
            logger.error('Prometheus: type(labels) must be dict')
            return
        if type(metrics) is not dict:
            logger.error('Prometheus: type(metrics) must be dict')
            return
        if type(prefix) is not str:
            logger.error('Prometheus: type(metrics) must be str')
            return
        recursive_processing(gauges=gauges, prefix=prefix, metrics=metrics, labels=labels)

    def start(self, *args, **kwargs):
        if not self.config.prometheus.enabled:
            logger.info(f'Prometheus: disabled in configuration')
            return
        server: Thread = Thread(target=self.start_http_server, name='prometheus_http_server')
        server.start()
        logger.info('Prometheus: server launched')
        while True:
            data = self.queue.get(block=True, timeout=None)
            if type(data) is not dict:
                raise Exception('Prometheus: unknown data in Queue')
            self.update_stats(**data)
            # logger.debug(f"Prometheus: served new stats")

    def start_http_server(self):
        class HandlerClass(BaseHTTPRequestHandler):
            def do_GET(self):
                self.send_response(200)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                body = ''.join([f"{g}\n" for g in gauges.values()])
                self.wfile.write(body.encode())

            def log_message(self, *args, **kwargs):
                pass
        httpd = HTTPServer(('', self.config.prometheus.port), HandlerClass)
        httpd.serve_forever()
