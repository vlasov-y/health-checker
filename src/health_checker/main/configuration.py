#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Read about environment variables in README.md

from datetime import datetime
from os import getenv
from contextlib import suppress
from typing import Dict, List

import json
import yaml

from .abstract import ASingleton, AConfigurationBlock
from .logger import get_logger

logger = get_logger()


class Configuration(metaclass=ASingleton):
    singleton_object = None

    class Smtp(AConfigurationBlock):
        def __init__(self, **kwargs):
            self.server: str = kwargs.get('server')
            self.port: int = int(kwargs.get('port', 465))
            self.user: str = kwargs.get('user')
            self.password: str = kwargs.get('password')
            self.email: str = self.user if '@' in self.user else f"{self.user}@{self.server}"

    class Telegram(AConfigurationBlock):
        def __init__(self, **kwargs):
            self.api_key: str = kwargs.get('api_key')

    class Prometheus(AConfigurationBlock):
        def __init__(self, **kwargs):
            self.port: int = kwargs.get('port')
            self.enabled: bool = kwargs.get('enabled')

    class Notification(AConfigurationBlock):
        def __init__(self, **kwargs):
            if getenv('HC_NOTIFICATION', None):
                self.enabled = getenv('HC_NOTIFICATION').lower() in ['yes', '1', 'true']
            else:
                self.enabled: bool = kwargs.get('enabled')
            self.interval: int = kwargs.get('interval')
            self.ssl_renew: bool = kwargs.get('ssl_renew')
            self.next_notification_after: datetime = datetime.utcnow()

    class Checking(AConfigurationBlock):
        def __init__(self, **kwargs):
            self.interval: int = kwargs.get('interval')
            self.fails_max: int = kwargs.get('fails_max')
            self.success_min: int = kwargs.get('success_min')
            self.ssl_min_days: int = kwargs.get('ssl_min_days')
            self.ssl_check_interval: int = kwargs.get('ssl_check_interval')

    def __str__(self):
        def converter(obj):
            if isinstance(obj, datetime):
                return str(obj)
            elif issubclass(type(obj), AConfigurationBlock):
                return obj.__dict__
        return json.dumps(self.__dict__, sort_keys=True, indent=2, default=converter)

    def update(self, *, base: dict, upd: dict) -> dict:
        for k, v in upd.items():
            if isinstance(v, dict):
                base[k] = self.update(base=base.get(k, {}), upd=v)
            else:
                base[k] = v
        return base

    def __init__(self):
        logger.debug(f'Configuration: creating object: {hash(self)}')
        try:
            with open(getenv('HC_DEFAULT_CONFIG_PATH', '/no-config'), 'rt') as f:
                y = yaml.safe_load(f)
            with suppress(FileNotFoundError):
                with open(getenv('HC_CONFIG_PATH', '/config.yaml'), 'rt') as f:
                    logger.info(f'Configuration: loading config {f.name}')
                    config = yaml.safe_load(f)
                    y = self.update(base=y, upd=config)
            # validation
            assert y.get('hosts'), 'Configuration: No hosts specified'
            assert y.get('recipients'), 'Configuration: No recipients specified'
            assert type(y.get('general')) is dict, 'Configuration: .general is absent or is not dict'
            assert y['general'].get('notification'), 'Configuration: .general.notification is absent'
            assert y['general'].get('checking'), 'Configuration: .general.checking is absent'
            # creation of config objects
            self.smtp = self.Smtp(**y['general'].get('smtp', {}))
            self.telegram = self.Telegram(**y['general'].get('telegram', {}))
            self.prometheus = self.Prometheus(**y['general'].get('prometheus', {}))
            self.notification = self.Notification(**y['general'].get('notification', {}))
            self.checking = self.Checking(**y['general'].get('checking', {}))
            self.recipients: Dict[str, dict] = y['recipients']
            self.groups: Dict[str, list] = y.setdefault('groups', {})
            self.hosts: Dict[str, dict] = y['hosts']
            logger.debug(self)
        except AttributeError as e:
            logger.error(f'Configuration: initialization error: {str(e)}')
            exit(1)
