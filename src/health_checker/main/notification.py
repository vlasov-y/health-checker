#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP_SSL

import markdown
import requests
import telegram

from .abstract import ANotificationChannel
from .configuration import Configuration
from .group import Group
from .logger import get_logger

logger = get_logger()


class NotificationChannel:
    def __new__(cls, **kwargs) -> ANotificationChannel:
        """
        Gets type of notification channel from kwargs and forward other options to constructor of new object
        :param kwargs:
        :return: ANotificationChannel object
        """
        channel_type = kwargs.pop('type').lower()
        types = dict(
            email=ChannelEmail,
            slack=ChannelSlack,
            telegram=ChannelTelegram
        )
        assert channel_type in types.keys(), f'Unknown notification channel type {channel_type}'
        return types[channel_type](**kwargs)


class ChannelEmail(ANotificationChannel):
    def __init__(self, **kwargs):
        super().__init__()
        self.address: str = kwargs.pop('address')

    def notify(self, subject: str, body: str, **kwargs):
        if not self.enabled or not self.config.smtp:
            return
        try:
            if kwargs.get('markdown'):
                body = markdown.markdown(body, output_format="html5")
            else:
                patterns = (
                    (r'^(\s*)([^ ]+):', r'\1<b>\2</b>:'),
                    (' ', '&nbsp')
                )
                lines = body.split('\n')
                for i in range(len(lines)):
                    for p in patterns:
                        lines[i] = re.sub(p[0], p[1], lines[i])
                body = MIMEText('<br>'.join(lines), 'html')
            msg = MIMEMultipart()
            msg.attach(body)
            msg.add_header('Subject', subject)
            msg.add_header('From', self.config.smtp.email)
            try:
                logger.debug(f'Sending email to {self.address}')
                mailer = SMTP_SSL(self.config.smtp.server, self.config.smtp.port)
                mailer.login(self.config.smtp.user, self.config.smtp.password)
            except Exception as e:
                logger.error(f'SMTP connection refused: {str(e)}')
                return
            mailer.sendmail(from_addr=self.config.smtp.user, to_addrs=[self.address], msg=msg.as_string())
            mailer.close()
        except Exception as e:
            logger.error(f'Email sending error: {str(e)}')


class ChannelSlack(ANotificationChannel):
    def __init__(self, **kwargs):
        super().__init__()
        self.webhook_url: str = kwargs.pop('webhook_url')

    def notify(self, subject: str, body: str, **kwargs):
        if not self.enabled:
            return
        msg = f'{subject.replace("#", ">")}\n{body.replace("**", "")}'
        try:
            logger.debug(f'Sending Slack notification: {self.webhook_url}')
            requests.post(self.webhook_url, json=dict(text=msg))
        except Exception as e:
            logger.error(f'Slack sending error: {str(e)}')


class ChannelTelegram(ANotificationChannel):
    markdown_patterns = [
        (re.compile(r'\*\*'), '__'),
        (re.compile(r'\*'), '_'),
        (re.compile(r'__'), '*'),
        (re.compile(r'#\s?([^\n]+)'), '*\\g<1>*'),
        (re.compile(r'([\[\]])'), '\\g<1>')
    ]
    
    def __init__(self, **kwargs):
        super().__init__()
        self.chat_id: str = kwargs.pop('chat_id')
        self.bot = telegram.Bot(token=self.config.telegram.api_key)

    def notify(self, subject: str, body: str, **kwargs):
        if not self.enabled or not self.config.telegram:
            return
        text = f'__{subject.replace("-", ".")}__\n```\n{body}\n```'
        for regexp, replace in self.markdown_patterns:
            text = regexp.sub(replace, text)
        try:
            self.bot.send_message(chat_id=self.chat_id, parse_mode=telegram.ParseMode.MARKDOWN, text=text)
            logger.debug(f'Telegram sent message: {self.chat_id}')
        except Exception as e:
            logger.error(f'Telegram sending error: {str(e)}')


def send_notification(*groups: Group, subject: str, body: str, **kwargs) -> None:
    """
    Send notification to all channels for all users in groups
    :param groups: Member groups to notify
    :param subject: Subject of the message
    :param body: Body with description
    :param kwargs: Additional arguments [optional]
    :return: None
    """
    for group in groups:
        for member in group.members:
            for channel in member.channels:
                channel.notify(subject=subject, body=body, **kwargs)
