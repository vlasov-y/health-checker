#!/usr/bin/env python
# -*- coding: utf-8 -*-

import copy
import re
import signal
import socket
import ssl
import yaml
from datetime import datetime, timedelta
from enum import Enum
from multiprocessing import Queue
from subprocess import check_call, CalledProcessError, DEVNULL
from time import sleep
from typing import List

from .abstract import AProcess, AProducer
from .configuration import Configuration
from .group import Group
from .notification import send_notification
from .logger import get_logger

logger = get_logger()


class HostCheck:
    class Type(Enum):
        PING = 1
        CURL = 2

    def __init__(self, *urls, check_type: Type, config: Configuration.Checking):
        """
        :param check_type: Type of subsystem
        :param config: Checking config for this host (including all overwrites)
        """
        self.check_type = check_type
        check_functions = dict(
            curl=self.__curl,
            ping=self.__ping
        )
        self.check_function = check_functions[self.name]
        self.urls = sorted(urls)
        self.config = config
        # counters and flags
        self.enabled: bool = bool(self.urls)
        self.fails_count: int = 0
        self.successes_count: int = self.config.success_min
        self.__last_time_seen_online: datetime = datetime.utcnow()
        self.online: bool = True
        self.failed_on_url = ''

    @property
    def name(self):
        return self.check_type.name.lower()

    @property
    def last_successful_check(self):
        return self.__last_time_seen_online.strftime('%A, %d %b at %H:%M:%S')

    def check(self, *args, **kwargs):
        """ Call checking function with data """
        if not self.check_function():
            self.fails_count += 1
            if self.fails_count >= self.config.fails_max:
                self.successes_count = 0
                self.online = False
        else:
            if self.successes_count < self.config.success_min:
                self.successes_count += 1
                if self.successes_count == self.config.success_min and not self.online:
                    self.fails_count = 0
                    self.online = True
                    self.__last_time_seen_online = datetime.utcnow()

    def __bool__(self):
        """ Returns True if check succeeded (host online) """
        return self.online

    def __str__(self):
        if self:
            y = yaml.safe_dump({
                self.name: {
                    'online': self.online,
                    'urls': self.urls
                }
            })
        else:
            y = yaml.safe_dump({
                self.name: {
                    'online': self.online,
                    'urls': self.urls,
                    'fails_count': self.fails_count,
                    'last_success': f"{self.last_successful_check} UTC",
                    'checking_interval': str(timedelta(seconds=self.config.interval))
                }
            })
        return y.replace('-', '  -')

    def export_metrics(self) -> dict:
        return dict(
            fails_count=self.fails_count,
            successes_count=self.successes_count,
            last_successful_check=self.last_successful_check,
            online=self.online
        )

    def __curl(self) -> bool:
        for url in self.urls:
            logger.debug(f"curl: {url}")
            try:
                check_call(f"curl -sSL --connect-timeout 5 '{url}'", stdout=DEVNULL, shell=True)
            except CalledProcessError as e:
                logger.error(f"curl: failed with exitcode {e.returncode} > {url}")
                self.failed_on_url = url
                return False
        return True

    def __ping(self) -> bool:
        for url in self.urls:
            logger.debug(f"ping: {url}")
            try:
                check_call(f"ping -c 2 -W 2 '{url}'", stdout=DEVNULL, shell=True)
            except CalledProcessError as e:
                logger.error(f"ping: failed with exitcode {e.returncode} > {url}")
                self.failed_on_url = url
                return False
        return True


class Host(AProcess, AProducer):
    class State(Enum):
        ONLINE = 'ok'
        OFFLINE = 'fail'
        PARTLY = 'warn'

    def __init__(self, name: str, queue: Queue = None):
        AProcess.__init__(self)
        AProducer.__init__(self, queue=queue)
        self.exit = False
        self.host_settings: dict = self.config.hosts.get(name)
        assert self.host_settings, f"No host config found for {name}"
        self.name = name
        # ssl urls list
        self.ssl = dict(
            urls=[
                dict(url=re.sub('^https?://', '', url), expire_at=None)
                for url in self.host_settings.setdefault('ssl', [])
            ],
            next_check_after=datetime.utcnow()
        )
        # notification settings
        notification_list = self.host_settings.setdefault('notify', self.config.groups.keys())
        self.notification_groups = [Group(name=name) for name in notification_list]
        # composing checking and notification policies (making copies in order not to overwrite global objets)
        self.checking: Configuration.Checking = copy.copy(self.config.checking)
        self.checking.update_dict(self.host_settings.get('checking'))
        self.notification: Configuration.Notification = copy.copy(self.config.notification)
        self.notification.update_dict(self.host_settings.get('notification'))
        # create checking subsystems
        for i in ('curl', 'ping'):
            if i in self.host_settings and type(self.host_settings.get(i, None)) is not list:
                self.host_settings[i] = [self.host_settings[i]]
        self.subsystems = [
            HostCheck(*self.host_settings.get('curl', []), check_type=HostCheck.Type.CURL, config=self.checking),
            HostCheck(*self.host_settings.get('ping', []), check_type=HostCheck.Type.PING, config=self.checking)
        ]
        # disabling unused subsystems
        for subs in self.subsystems:
            if not subs.enabled:
                # delete subsystem info in case if it has no configured data from YAML
                self.subsystems.remove(subs)
        # used for controlling notifications about going to offline
        self.current_state: dict = dict(
            online_subsystems_count=len(self.subsystems),
            state=Host.State.ONLINE
        )
        self.previous_state: dict = copy.copy(self.current_state)
        self.next_notification_after: dict = {subs: datetime.utcnow() for subs in self.subsystems}

    def _ssl(self):
        def _ssl_get_expiration_datetime(url: str):
            context = ssl.create_default_context()
            connection = context.wrap_socket(
                socket.socket(socket.AF_INET),
                server_hostname=url
            )
            connection.settimeout(5.0)
            # TODO Implement opportunity to specify SSL request port for not standard URLs
            connection.connect((url, 443))
            ssl_info = connection.getpeercert()
            # parse the string from the certificate into a Python datetime object
            expire_datetime = datetime.strptime(ssl_info['notAfter'], r'%b %d %H:%M:%S %Y %Z')
            return expire_datetime

        if not self.ssl['urls'] or datetime.utcnow() < self.ssl['next_check_after']:
            return
        self.ssl['next_check_after'] = datetime.utcnow() + timedelta(days=self.checking.ssl_check_interval)
        # for each url
        for index, d in enumerate(self.ssl['urls']):
            # get expiration datetime
            logger.debug(f"SSL: checking {d['url']}")
            check_status = dict(
                succeeded=False,
                message=''
            )
            try:
                expire_at = _ssl_get_expiration_datetime(url=d['url'])
                check_status['succeeded'] = True
            except Exception as e:
                logger.error(f"Host: SSL check exception '{d['url']}': {str(e)}")
                check_status['succeeded'] = False
                check_status['message'] = str(e)
            # parsing results
            if not check_status['succeeded']:
                # message about checking error
                subject = f"# SSL certificate check failed! URL: {d['url']}"
                body = f"""
Got unknown error while checking certificate!  
*URL*: **{d['url']}**  
*Message*: `{check_status['message']}`  """
            else:
                logger.debug(f"SSL: {d['url']} expire at {expire_at}")
                if not d['expire_at']:
                    self.ssl['urls'][index]['expire_at'] = expire_at
                    d['expire_at'] = expire_at
                time_delta = expire_at - datetime.utcnow()
                logger.debug(f'Checking: time_delta {time_delta} < limit {timedelta(days=self.checking.ssl_min_days)}')
                # check expiration
                if time_delta < timedelta(days=self.checking.ssl_min_days):
                    if time_delta == 0:
                        # if expired already
                        subject = f"# SSL certificate expired! URL: {d['url']}"
                        body = f"""
SSL expired!  
*URL*: **{d['url']}**  
*Expired at*: **{expire_at} UTC**  """
                        logger.error(f"Host: {d['url']} SSL expired at {expire_at}")
                    else:
                        # if expire soon
                        subject = f"# SSL certificate expire soon! URL: {d['url']}"
                        body = f"""
SSL expire soon!  
*URL*: **{d['url']}**  
*Will expire at*: **{expire_at} UTC**  
*Days left*: **{time_delta.days} days**  """
                        logger.error(f"Host: {d['url']} SSL expire soon at {expire_at}")
                else:
                    self.ssl['urls'][index]['expire_at'] = expire_at
                    # if old expiration date does not equal to new one
                    if d['expire_at'] != expire_at and self.notification.ssl_renew:
                        logger.info(f"Host: {d['url']} SSL renewed to {expire_at}")
                        # then certificate was renewed
                        subject = f"# SSL certificate renewed! URL: {d['url']}"
                        body = f"""
SSL certificate was renewed!  
*URL*: **{d['url']}**  
*New certificate will expired at*: **{expire_at} UTC**    
*Days left*: **{time_delta.days} days**  """
                    else:
                        # continue in case if certificate is alright
                        continue
            # notify if something is wrong
            send_notification(*self.notification_groups, subject=subject, body=body)

    def update_state(self):
        """
        Update current state considering chages in subsystems statuses
        :return: sets self.state
        """
        del self.previous_state
        self.previous_state = copy.copy(self.current_state)
        self.current_state['online_subsystems_count']: int = len([1 for s in self.subsystems if s.online])
        if self.current_state['online_subsystems_count'] == len(self.subsystems):
            self.current_state['state'] = Host.State.ONLINE
        elif self.current_state['online_subsystems_count'] == 0:
            self.current_state['state'] = Host.State.OFFLINE
        else:
            self.current_state['state'] = Host.State.PARTLY

    def notify(self) -> None:
        """
        Send notification about all subsystems
        :return: None
        """
        if self.current_state['state'] == Host.State.ONLINE and self.previous_state['state'] == self.current_state['state']:
            # then nothing had happened
            return
        elif self.current_state['state'] == Host.State.ONLINE and self.previous_state['state'] != self.current_state['state']:
            # host become online
            self.next_notification_after: dict = {subs: datetime.utcnow() for subs in self.subsystems}
        else:
            # some subsystem is not online but nothing have changed
            failed_subsystems = [subs for subs in self.subsystems if not subs.online]
            if self.current_state['online_subsystems_count'] != self.previous_state['online_subsystems_count']:
                pass
            elif True not in [datetime.utcnow() >= dt for subs, dt in self.next_notification_after.items() if subs in failed_subsystems]:
                return
            for subs in failed_subsystems:
                self.next_notification_after[subs] = datetime.utcnow() + timedelta(minutes=self.notification.interval)

        subject = f'[ {self.current_state["state"].value} ] HealthCheck {self.name}'
        body = '\n'.join([str(s) for s in self.subsystems])
        logger.debug(f'Notification subject: {subject}')
        send_notification(*self.notification_groups, subject=subject, body=body)

    def signal_handler(self, signum, frame):
        self.exit = True

    def update_prometheus_metrics(self):
        if not self.config.prometheus.enabled or not self.queue:
            return
        for subs in self.subsystems:
            self.queue.put(dict(
                metrics=subs.export_metrics(),
                labels=dict(host=self.name, subsystem=subs.name)
            ))

    def __call__(self, *args, **kwargs):
        self.start(*args, **kwargs)

    def start(self, *args, **kwargs):
        # handle signals
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)
        # start main algorithm
        logger.info(f'Host: watching {self.name}')
        while True:
            # exit handler
            if self.exit:
                exit(0)
            for subsystem in self.subsystems:
                was_online = subsystem.online
                # make a check
                subsystem.check()
                if not subsystem.online and was_online:  # if gone offline
                    logger.debug(f"Host: {self.name} went offline {subsystem.name}")
                elif not was_online and subsystem.online:   # if came back online
                    logger.debug(f"Host: {self.name} become online {subsystem.name}")
            self.update_state()
            self.notify()
            # ssl
            if self.ssl['urls']:
                self._ssl()
            self.update_prometheus_metrics()
            sleep(self.checking.interval)
