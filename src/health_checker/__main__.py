#!/usr/bin/env python3

import signal
import sys
from os import getenv
from multiprocessing import Process, Queue
from os import getpid
from time import sleep
from typing import List

import psutil

from .main import Host, get_logger, AProcess, Prometheus


class SignalHandler:
    def __init__(self):
        signal.signal(signal.SIGINT, self.handler)
        signal.signal(signal.SIGTERM, self.handler)
        self.signal: int = -1

    @property
    def exit(self):
        return self.signal > 0

    def handler(self, signum: int, frame):
        self.signal = signum


class Main(AProcess):
    def __init__(self):
        super().__init__()
        self.signal_handler = SignalHandler()
        # objects
        self.logger = get_logger()
        # sub processes functions
        def host_serving_entrypoint(*args, **kwargs): Host(*args, **kwargs).start()

        def prometheus_entrypoint(*args, **kwargs): Prometheus(*args, **kwargs).start()

        self.logger.info(f'Hosts quantity: {len(self.config.hosts)}')
        self.prometheus_queue = Queue(maxsize=4096) if self.config.prometheus.enabled else None
        self.processes: List[Process] = [
            Process(target=host_serving_entrypoint, kwargs=dict(name=name, queue=self.prometheus_queue), daemon=True,
                    name=name)
            for name in self.config.hosts.keys()
        ]
        if self.config.prometheus.enabled:
            self.processes.append(
                Process(target=prometheus_entrypoint, kwargs=dict(queue=self.prometheus_queue),
                        daemon=True, name='prometheus')
            )

    def __call__(self, *args, **kwargs):
        return self.start(*args, **kwargs)

    def terminate_processes(self) -> int:
        exitcode: int = 0
        for p in self.processes:
            if isinstance(p.exitcode, int) and p.exitcode != 0:
                self.logger.error(f'Process "{p.name}" failed with exitcode {p.exitcode}')
                exitcode = p.exitcode
        # killing subprocesses
        parent = psutil.Process(getpid())
        for subp in parent.children(recursive=True):
            self.logger.debug(f'Terminating {subp}')
            subp.kill()
            self.logger.debug(f'Terminated {subp}')
        self.logger.debug('Finished termination')
        return exitcode

    def start(self, *args, **kwargs) -> int:
        self.logger.info(f'Program started')
        for process in self.processes:
            process.start()
        while True:
            if self.signal_handler.exit:
                self.logger.debug('Main: Caught interrupt')
                self.logger.info('Program finished')
                return self.terminate_processes()
            for p in self.processes:
                if not p.is_alive():
                    self.logger.debug(f'Main: subprocess {p.name} failed - terminating others')
                    self.logger.info('Program finished')
                    return self.terminate_processes()
            sleep(2)


def entrypoint():
    try:
        sys.exit(Main()())
    except Exception as e:
        if getenv('HC_DEBUG', 'false').lower() in ['1', 'true', 'yes']:
            raise e
        else:
            get_logger().error(f'Main: {str(e)}')


if __name__ == '__main__':
    entrypoint()
