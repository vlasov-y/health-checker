# Brief

Simple application on Python3 which proceed health checks to specified URLs.  
Use `ping` and `curl`. Check SSL certificates expiration.

# Environment variables
All configuration is made via environment variables.

| Name | Default | Description |
| ---- | ------- | ----------- |
| HC_CONFIG_PATH |  | Path to configfile |
| HC_DEBUG | false | Set to true or 1 for more verbosity (case insensitive) | 
| HC_DISABLE_NOTIFICATION | false | Set to true or 1 in order to disable all notification (case insensitive) |

# Configuration
```yaml
# general settings
general:
  # prometheus metrics settings
  prometheus:
    # general.prometheus.enabled: bool
    enabled: true
    # general.notification.port: int
    port: 5800
  # global notification settings
  notification:
    # general.notification.enabled: bool
    enabled: true
    # general.notification.interval: int
    # time in MINUTES before new notifications in case if service is still offline
    interval: 5
    # general.notification.ssl_renew: bool
    # if true, then it will send notification about renewing of certificate
    ssl_renew: true
  # global checking system settings
  checking:
    # global.checking.fails_max: int
    # how many failures must be one by one for considering host offline
    fails_max: 5
    # global.checking.success_min: int
    # how many successful replies should be received for considering host online again
    success_min: 2
    # global.checking.interval: int
    # time in seconds which describe how frequently do tests
    interval: 10
    # global.checking.ssl_min_days: int
    # consider SSL as expired if it has less days than specified there
    ssl_min_days: 7
    # general.checking.ssl_check_interval: int
    # days before checks of SSL certificates
    ssl_check_interval: 1
  # smtp settings
  smtp:
    # smtp.server: str = None
    server: my.smtp.server.com
    # smtp.port: int = 465
    port: 465
    # smtp.user: str = None
    user: user@my.smtp.server.com
    # smtp.password: str = None
    password: qwerty123456
  # telegram settings
  telegram:
    # telegram.api: str = None - Bot API key
    api_key: qwerty123456

# list of recipients that can be notified
# notice: name of recipients is used for list in groups
# there is must not be recipients with exactly the same name as one of the group
recipients:
  john-smith:
    - type: email
      address: j.smith@mail.com
    - type: slack
      webhook_url: https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX
  tom-jones:
    - type: telegram
      chat_id: there_should_be_chat_id
    - type: slack
      webhook_url: https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX
  someone-else:
    - type: slack
      webhook_url: https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX
    - type: slack  # notify to many channels at once
      webhook_url: https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX
  admins-chat:
    - type: telegram
      webhook_url: there_should_be_chat_id
    - type: slack
      webhook_url: https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX

# list of groups to notify
groups:
  everyone: # name of group
    - _all  # _all - is special code means all existent recipients
  some-team:
    - john-smith  # members list
    - someone-else
  dream-team:
    - admins-chat
  another-one-group:
    - tom-jones
    - someone_else

# list of hosts to check
hosts:
  some-host1:
    # either url or ping must exist, or you can specify both of them
    curl: http://hello.com/healthz # url to curl
    ping: hello.com # host to ping
    ssl:  # list of URL which must used for SSL certificate expiration checks
      - hello1.com
      - home.com
      - smth.com
    # list of groups to notify. Only groups are accepted, not single recipients
    # if you won't specify notify section or it will be empty, then all groups will be notified
    notify:
      - dream-team
      - another-one-group
    checking: # overrides global.checking (you can specify only needed parameters, others will be from global)
      interval: 20
      success_min: 2
      ssl_min_days: 10
    notification: # overrides global.notification (you can specify only needed parameters, others will be from global)
      ssl_renew: false

  another-site:
    curl: http://hello.com/healthz
    ping: hello.com
    notify:
      - tom-jones
    checking:
      interval: 20
      success_min: 2

  very-significant:
    curl: http://test.com/healthz
    ping: test.com
    notify:
      - _all # _all - is special code means all existent groups
    checking:
      interval: 45
      success_min: 10
```

