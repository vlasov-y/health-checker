.DEFAULT_GOAL := all
.PHONY: all build install clean clean-all uninstall

all: build install clean-all

build:
	python3 -m pip install wheel
	python3 setup.py bdist_wheel

install:
	python3 -m pip install --upgrade `find ./dist/ -type f -name '*.whl'`

uninstall:
	python3 -m pip uninstall health-checker

clean:
	python3 setup.py clean --all
	rm -rf src/*.egg-info

clean-all: clean
	rm -rf dist
