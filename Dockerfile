FROM python:3.8.5-slim AS builder
WORKDIR /app
COPY src/ ./src
COPY setup.py ./setup.py
COPY Makefile ./Makefile
RUN apt-get update && apt-get install -qqy make && \
    make build clean && \
    apt-get clean && \
    rm -rf /var/cache/apt

FROM python:3.8.5-alpine AS runner
WORKDIR /app
ENTRYPOINT [ "tini", "--", "health-checker" ]
ENV HC_DEFAULT_CONFIG_PATH ./default.yml
ENV USER mortal
RUN adduser -u 1000 -s /bin/false -D -H "$USER" && \
    chown -Rv "${USER}:${USER}" /app && \
    python3 -m pip install --upgrade pip && \
    apk add --update --no-cache curl tini && \
    chmod u+s `which ping`
COPY config/default.yaml $HC_DEFAULT_CONFIG_PATH

COPY --from=builder /app/dist/*.whl .
RUN apk add --update --no-cache --virtual .build-deps gcc musl-dev openssl-dev libffi-dev && \
    for i in *.whl; do python3 -m pip install $i; done && \
    apk del .build-deps
USER $USER
